// @codekit-prepend "respuestas.js"

var las_evaluaciones = document.querySelectorAll(".js-evaluacion");
var resultados = resultados;
var preguntas_num = 7;
var pregs = [
	"¿Qué tipo de piezas te gustó más?",
	"¿Qué imagen te pareció más clara?",
	"¿Qué tema te interesó más?",
	"¿Qué actividad te ayudó a entender mejor la información?",
	"¿Qué acción de mantenimiento te parece más importante?",
	"¿Qué acción de mantenimiento piensas hacer primero?"
];

function insertAfter(newNode, referenceNode)
{
	referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

/*
function obtener_localStrge () {
	for(var i=0,j=preguntas_num; i<j; i++)
	{
		var array_pregunta = JSON.parse(localStorage.getItem( "resultados_ev_"+i ));
		resultados.push(array_pregunta);
	}
}
*/


function personas_function () {
	var personas = [];

	var div_persona = document.querySelector(".js-persona");
	var encabezado_resultados = document.querySelector(".js-encabezado");

	// Crear Objeto Evaluación
	function Persona(i) {
		this.resp = [];
		// Salvar el orden de los elementos de cada evaluación
		this.agrupar = function()
		{
			for(var k=1, l=preguntas_num; k<=l; k++)
			{
				if (resultados[k])
				{
					for(var m=0, n=resultados[k].length; m<=n; m++)
					{
						if (resultados[k][m])
						{
							if (resultados[k][m].Nombre === this.nombre)
							{
								// console.log(resultados[k][m].Respuestas);
								this.resp.push(resultados[k][m].Respuestas);
							}
						}
					}
				}
			}
		};
		this.insertar_datos = function ()
		{
			this.encabezado = this.div_persona.querySelector(".js-nombre");
			this.cuerpo_resp = this.div_persona.querySelector(".js-resultados");

			var cuerpo;

			
			for( var o=0, p=this.resp.length; o<p ; o++ )
			{
				// Arreglar aquí
				 cuerpo = (o === 0) ? cuerpo+"<ul>" : cuerpo;

				for( var q=0, r=this.resp[o].length; q<r ; q++ )
				{

					cuerpo = ( q === 0) ? cuerpo+"<h3>"+pregs[o]+"</h3><li>" : cuerpo;

					cuerpo = cuerpo + "<span class='orden_respuesta'>" + (q + 1) + ": " + this.resp[o][q] + "</span>";

					cuerpo = ( q === ( this.resp[o].length - 1) ) ? cuerpo+"</li>" : cuerpo;

				}
				// Arreglar aquí
				cuerpo = (o === this.resp.length-1 ) ? cuerpo+"</ul>" : cuerpo;
			}
			this.cuerpo_resp.innerHTML = cuerpo;


			this.encabezado.innerHTML = this.nombre;
		};
		this.imprimir_persona = function () {
			this.div_persona = div_persona.cloneNode(true);
			this.nombre = resultados[0][i].Nombre;
			this.div_persona.setAttribute("data-persona", this.nombre);
			insertAfter(this.div_persona, encabezado_resultados);
		};
	}

	for (var j in resultados[0] )
	{
		personas[j] = new Persona(j);
		personas[j].imprimir_persona();
		personas[j].agrupar(j);
		personas[j].insertar_datos();
	}
}

function estadisticas () {
	// var preguntas = [];
	var resps = [ 
		[ ["","","",""],["","","",""],["","","",""],["","","",""] ],
		[ ["","","",""],["","","",""],["","","",""],["","","",""] ],
		[ ["","","",""],["","","",""],["","","",""],["","","",""] ],
		[ ["","","",""],["","","",""],["","","",""],["","","",""] ],
		[ ["","","",""],["","","",""],["","","",""],["","","",""] ],
		[ ["","","",""],["","","",""],["","","",""],["","","",""] ],
		[ ["","","",""],["","","",""],["","","",""],["","","",""] ]
	];
	var sumas = [ 
		[ [0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0] ],
		[ [0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0] ],
		[ [0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0] ],
		[ [0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0] ],
		[ [0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0] ],
		[ [0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0] ],
		[ [0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0] ]
	];

	// Crear Objeto Evaluación
	function agrupar(k)
	{
		if (resultados[k])
		{
			for(var m=0, n=resultados[k][0].Respuestas.length; m<n; m++)
			{
				for(var o=0, p=resultados[0].length; o<p; o++)
				{
					if ( resultados[k][o] )
					{
						// console.log(resultados[k][o].Respuestas[m]);
						var num_respuestas = resps[k][m].length;
						// var num_respuestas = 4;

						for(var q=0, r=num_respuestas; q<r; q++)
						{
							// var contador = 0;

							// console.log("k: " + (k-1) + ", m: " + m + ", o: " + o + ", q: " + q );

							// console.log( "resps[k][m][q]: " + resps[k][m][q] + ", " + "resultados[k][o].Respuestas[m]: " + resultados[k][o].Respuestas[m] );

							if ( resultados[k][o].Respuestas )
							{
								if ( resps[k][m][q] === resultados[k][o].Respuestas[m] )
								{
									// console.log("Ya estaba guardado: " + resps[k][m][q]);
									sumas[k][m][q]++;
									// console.log("sumas[k][m][q]: " + sumas[k][m][q]);
									break;
								}
								else
								{
									if ( resps[k][m][q] === "" )
									{
										// console.log("No estaba guardado: " + resultados[k][o].Respuestas[m]);

										resps[k][m][q] = resultados[k][o].Respuestas[m];
										// resps[k][m].push( resultados[k][o].Respuestas[m] );
										var a = resps[k][m].indexOf(resultados[k][o].Respuestas[m]);
										// console.log("a: " + a);

										sumas[k][m][a]++;
										// console.log("sumas[k][m][a]: " + sumas[k][m][a]);
										// contador++;
										break;
									}
								}
							}
							// console.log("//");
						}
					}
				}
				// console.log("-------------------");
			}
		}
	}

	function imprimir_sumas () {
		var div_pregunta = document.querySelector(".js-pregunta");
		var estads = document.querySelector(".js-resultados-estadisticas");

		// for (var j=1, i=resultados.length; j<=i; j++)
		for (var j=0, i=sumas.length; j<i; j++)
		{
			var nueva_div_pregunta = div_pregunta.cloneNode(true);
			var titulo = nueva_div_pregunta.querySelector(".js-titulo");
			var contenido = nueva_div_pregunta.querySelector(".js-resultados");
			var pregunta = pregs[j];
			titulo.innerHTML = pregunta;

			for (var l=0, k=sumas[j].length; l<k; l++)
			{
				var div_est = document.createElement("div");
				div_est.classList.add("respuestas");
				div_est.setAttribute("data-orden", "Posición "+(l+1));
				contenido.appendChild(div_est);

				var ordenados = sumas[j][l];
				ordenados.sort(function(a, b){return b-a;}); 

				for (var n=0, m=sumas[j][l].length; n<m; n++)
				{

					var div_ord = document.createElement("div");
					div_ord.classList.add("respuesta");
					div_ord.setAttribute("data-numero", sumas[j][l][n]);
					div_ord.style.flexGrow = sumas[j][l][n];
					div_ord.setAttribute("data-respuesta", resps[j][l][n] + " (" + sumas[j][l][n] + ")");

					// Ordenar de mayor a menor por respuestas
					if (sumas[j][l][n] == ordenados[0])
					{
						div_ord.style.order = "1";
					}
					else if (sumas[j][l][n] == ordenados[1])
					{
						div_ord.style.order = "2";
					}
					else if (sumas[j][l][n] == ordenados[2])
					{
						div_ord.style.order = "3";
					}
					else if (sumas[j][l][n] == ordenados[3])
					{
						div_ord.style.order = "4";
					}
					div_est.appendChild(div_ord);
				}
			}
			estads.appendChild(nueva_div_pregunta);
			
		}
		div_pregunta.remove();

	}

	// for (var j=1, i=1; j<=i; j++)
	for (var j=1, i=resultados.length; j<=i; j++)
	{
		agrupar(j);
	}
	resps = resps.slice(1, resps.length);
	sumas = sumas.slice(1, sumas.length);
	// console.log( "resps: " + resps );
	// console.log( sumas );

	imprimir_sumas();

}

function estadisticas_sinformato () {

	Array.prototype.forEach.call(las_evaluaciones, function(ev,i){

		// Checar si existen localStorage para cada evaluación

		// Aquí va la opción para borrar los localStorage

		if ( resultados[i] )
		{
			var div_resultados = ev.querySelector(".js-resultados");
			// Poner todo el código para mostrar bonito los resultados de los arrays

			div_resultados.innerHTML = JSON.stringify(resultados[i]);
		}

	});
	
}


document.addEventListener('DOMContentLoaded', function()
{

	//obtener_localStrge();

	var resultados_estadisticas = document.querySelector(".js-resultados-estadisticas");
	var resultados_personas = document.querySelector(".js-resultados-personas");
	var resultados_sin_formato = document.querySelector(".js-resultados-sin_formato");

	if ( resultados_estadisticas )
	{
		estadisticas();
	}
	if ( resultados_personas )
	{
		personas_function();
	}
	if ( resultados_sin_formato )
	{
		estadisticas_sinformato();
	}
});