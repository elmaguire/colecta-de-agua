// Codigo js para página documental interactivo
// @codekit-prepend "librerias/jquery.js"
// @codekit-prepend "librerias/jquery-ui.min.js"
// @codekit-prepend "librerias/jquery.ui.touch-punch.min.js"

// var ancho_pag = document.body.offsetWidth;
var btn_menu = document.querySelector(".js-btn-menu");
var contenedor = document.querySelector(".js-contenedor");
var diapo_visible = 'transforma';
// var diapositivas = contenedor.children;
var el_menu = document.querySelector(".js-navegacion");
var menu = document.querySelector(".js-menu");
// var nums_diapositivas = [];
var secciones = document.querySelectorAll(".js-seccion");
var diapo_actual;
var las_evaluaciones = document.querySelectorAll(".js-evaluacion");
var evaluacion = [];


// Letrero para mostrar errores en ipad
// var ua = navigator.userAgent;
// var isiPad = /iPad/i.test(ua) || /iPhone OS 3_1_2/i.test(ua) || /iPhone OS 3_2_2/i.test(ua);
// var debug_ipad = document.getElementById('debug_ipad');

function hasClass(el, cls)
{
	return el.className && new RegExp("(\\s|^)" + cls + "(\\s|$)").test(el.className);
}

// Menú y navegación -->
 function mov_menu()
{
	btn_menu.classList.toggle("open");
	menu.classList.toggle("visible");
	el_menu.classList.toggle("visible");
}

function menu_func()
{
	if (btn_menu)
	{
		btn_menu.onclick = function() {
			mov_menu();
		};
	}
}

function diapo_actual_func () {
	Array.prototype.forEach.call(secciones, function(el){
		if( el !== diapo_actual && hasClass(el, "actual") )
		{
			el.classList.remove("actual");
		}
	});
	diapo_actual.classList.add("actual");
}

function menu_activo_func()
{
	var menu_activo = menu.querySelector("[data-seccion="+diapo_visible+"]");
	if (menu_activo)
	{
		Array.prototype.filter.call(menu_activo.parentNode.children, function(child){
			if( child !== menu_activo  && hasClass(child, "actual") )
			{
				child.classList.remove("actual");
			}
		});
		menu_activo.classList.add("actual");
	}
}

function navegacion()
{
	var btns_nav = document.querySelectorAll('.js-nav');

	Array.prototype.forEach.call(btns_nav, function(el)
	{
		el.onclick = function()
		{
			diapo_visible = el.getAttribute('data-ir');

			if (diapo_visible)
			{
				// Mover carrusel
				// var diapo = index_en_array( diapositivas, diapo_visible);
				// setTimeout(function()
				// {
				// 	contenedor.style.left = (diapo * ancho_pag) * ( - 1 ) + "px";
				// },250);
				// Menú activo
				menu_activo_func();

				diapo_actual = contenedor.querySelector("[data-seccion="+diapo_visible+"]");

				if (diapo_actual)
				{
					// Diapo actual
					diapo_actual_func();
				}
			}

			setTimeout(function()
			{
				el_menu.style.display = "block";
				btn_menu.classList.remove("internas");
			}, 250);

			if (  el_menu.classList.contains("visible") )
			{
				mov_menu();
			}
		};
	});
}

// <-- Menú y navegación

// Evaluación -->

var el_nombre;

function evaluaciones () {
	
	// Crear Objeto Evaluación
	function Evaluacion(ev, resultados_ev, i) {
		this.ev = ev;
		this.resultados = [];
		this.nombre = "";
		this.resultados_todo = [];
		this.palomita = this.ev.querySelector(".js-evaluacion--palomita");
		this.btn_guardar_nombre = this.ev.querySelector(".js-btn__guardar__nombre");
		this.btn_guardar_orden = this.ev.querySelector(".js-btn__guardar__orden");

		

		// Salvar el orden de los elementos de cada evaluación
		this.salvar = function() {

			if ( evaluacion[i].btn_guardar_nombre )
			{
				evaluacion[i].btn_guardar_nombre.addEventListener('click', function () {

					// Obtener objetos de la evaluación y poner el orden en un array resultados[]
					var input_nombre = evaluacion[i].ev.querySelector(".js-input--nombre");

					el_nombre = this.nombre = input_nombre.value;

					// Guardar este orden de objetos
					guardarInfo(el_nombre, i);

				});
			}

			if ( evaluacion[i].btn_guardar_orden )
			{
				evaluacion[i].btn_guardar_orden.addEventListener('click', function () {

					// Obtener objetos de la evaluación y poner el orden en un array resultados[]
					var objs_ordenados = evaluacion[i].ev.querySelectorAll(".js-eval__obj");

					Array.prototype.forEach.call(objs_ordenados, function(obj)
					{

						var orden = obj.getAttribute("data-original");
						evaluacion[i].resultados.push(orden);

					});

					// Guardar este orden de objetos
					guardarInfo(evaluacion[i].resultados, i, el_nombre);

				});
			}

			function guardarInfo(results, i, nombre){

				// Obtener información guardada del localStorage y ponerla en un array de este objeto
				evaluacion[i].resultados_todo = JSON.parse( localStorage.getItem(resultados_ev) );
				// Agregar los resultados de esta evaluación al array de este objeto

				function agregarRespuesta(rest, nom) {
					if (nom)
					{
						evaluacion[i].resultados_todo.push({
							Nombre: nom,
							Respuestas: rest
						});
					}
					else
					{
						evaluacion[i].resultados_todo.push({
							Nombre: rest
						});	
					}
				}

				agregarRespuesta(results, nombre);

				// 
				// console.log(evaluacion[i].resultados_todo);
				// Insertar el nuevo array con todos los resultados al localStorage
				localStorage.setItem(resultados_ev, JSON.stringify(evaluacion[i].resultados_todo));
				// Limpiar el array de orden de objetos de esta evaluación.
				evaluacion[i].resultados.length = 0;

				// Muestra palomita de que se guardó la información.
				evaluacion[i].palomita.classList.add("visible");

				// setTimeout(function(){
				// 	evaluacion[i].palomita.classList.remove("visible");
				// }, 1500);

			}
		};

	}

	$( ".js-evaluacion__ordenar" ).sortable(
	{
		stop: function(event, ui) {
			var orden = 0;
			$(this).find("figure").each(function () {
				$(this).attr("data-posicion", orden).find(".js-eval--numero").text(orden+1);
				orden++;
			});
		}
	});
	$( ".js-evaluacion__ordenar" ).disableSelection();

	Array.prototype.forEach.call(las_evaluaciones, function(ev,i){

		var resultados_ev = "resultados_ev_"+i;

		evaluacion[i] = new Evaluacion(ev, resultados_ev, i);
		evaluacion[i].salvar();

		// Checar si existen localStorage para cada evaluación, si no, crear un nuevo array

		// Aquí va la opción para borrar los localStorage

		// Checar todas las localStorage y borrar
		
		// Mostrar cada localStorage:
		/*
		for (var j = 0; j < localStorage.length; j++){
			
			console.log( localStorage.getItem(localStorage.key(i)) );
		}
		*/

		// Borrar todas las localStorage:
		// localStorage.clear();
		

		if ( localStorage.getItem(resultados_ev) )
		{
			console.log( resultados_ev + ": " + localStorage.getItem(resultados_ev));
		}
		else
		{
			localStorage.setItem(resultados_ev, JSON.stringify( [] ));
		}

	});
	

	// localStorage.setItem("Nombre", nom);
}

// <-- Evaluación


document.addEventListener('DOMContentLoaded', function()
{
	menu_func();
	navegacion();
	evaluaciones();
});