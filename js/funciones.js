// Codigo js para página documental interactivo
// @codekit-prepend "librerias/jquery.js"

// var ancho_pag = document.body.offsetWidth;
var btn_menu = document.querySelector(".js-btn-menu");
// var contenedor = document.querySelector(".js-contenedor");
// var diapositivas = contenedor.children;
var el_menu = document.querySelector(".js-navegacion");
var menu = document.querySelector(".js-menu");
// var nums_diapositivas = [];
// var secciones = document.querySelectorAll(".js-seccion");
var tOut = false;
var milSec = 500;
// var ctx, color = "#2074AD";
var diapo_actual = document.querySelector(".actual");
var diapo_visible = diapo_actual.getAttribute('data-destino');
var los_simuladores = document.querySelectorAll(".js-simulador");
var simulador = [];

var ua = navigator.userAgent;
var isiPad = /iPad/i.test(ua) || /iPhone OS 3_1_2/i.test(ua) || /iPhone OS 3_2_2/i.test(ua);

// Letrero para mostrar errores en ipad
// var debug_ipad = document.getElementById('debug_ipad');

// Generales  -->

$.fn.drawTouch = function(ctx_i)
{
	var start = function(e) {
		e = e.originalEvent;
		ctx_i.beginPath();
		x = e.changedTouches[0].pageX;
		y = e.changedTouches[0].pageY;
		ctx_i.moveTo(x,y);
	};
	var move = function(e) {
		e.preventDefault();
		e = e.originalEvent;
		x = e.changedTouches[0].pageX;
		y = e.changedTouches[0].pageY;
		ctx_i.lineTo(x,y);
		ctx_i.stroke();
	};
	$(this).on("touchstart", start);
	$(this).on("touchmove", move);	
}; 

$.fn.drawPointer = function(ctx_i)
{
	var start = function(e) {
		e = e.originalEvent;
		ctx_i.beginPath();
		x = e.pageX;
		y = e.pageY;
		ctx_i.moveTo(x,y);
	};
	var move = function(e) {
		e.preventDefault();
		e = e.originalEvent;
		x = e.pageX;
		y = e.pageY;
		ctx_i.lineTo(x,y);
		ctx_i.stroke();
    };
	$(this).on("MSPointerDown", start);
	$(this).on("MSPointerMove", move);
};		

$.fn.drawMouse = function(ctx_i)
{
	var clicked = 0;
	var start = function(e) {
		clicked = 1;
		ctx_i.beginPath();
		x = e.pageX;
		y = e.pageY;
		ctx_i.moveTo(x,y);
	};
	var move = function(e) {
		if(clicked){
			x = e.pageX;
			y = e.pageY;
			ctx_i.lineTo(x,y);
			ctx_i.stroke();
		}
	};
	var stop = function() {
		clicked = 0;
	};
	$(this).on("mousedown", start);
	$(this).on("mousemove", move);
	$(window).on("mouseup", stop);
};

function insertAfter(newNode, referenceNode)
{
	referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

function debounce (func, wait, immediate)
{
	var timeout;
	return function ()
	{
		var context = this, args = arguments;
		var later = function () {
			timeout = null;
			if( !immediate ) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply (context,args);
	};
}

/*
function disparar_click (elem) {

	var event = document.createEvent('HTMLEvents');
	event.initEvent('click', true, false);

	elem.dispatchEvent(event);
}
*/

function ipad_function () {
	if (isiPad){
		$("audio, video").css({"height": "0 !important", "width": "0 !important;"});
		$(".media--control").css({"opacity": 1});
	}
}

/*
var index_en_array = function(el_array, objetivo)
{
	for (var i = 0, len = el_array.length; i < len; i++)
	{
		if (el_array[i].getAttribute("data-destino") === objetivo)
		{
			return i;
		}
	}
	return null;
};
*/
/*
function newCanvas(){
	//define and resize canvas

	var win_height = $(window).height();
	var win_width = $(window).width();
	var los_canvas_dom = document.querySelectorAll('.js-canvas');

	var los_canvas0 = $( $('.js-canvas')[0] );
	if (los_canvas0)
	{

		los_canvas0.height( win_height );
		var canvas = '<canvas id="canvas0" width="' + win_width +'" height="'+ win_height +'"></canvas>';
		var btn_download1 = "<a href='' class='js-btn__descargar__1 btn__descargar' download='mapa_comunitario.png' target='_blank'>Guardar imagen</a>";
		los_canvas0.html(btn_download1 + canvas);

		var img_mirror = document.createElement("img");
		img_mirror.setAttribute("id", "mirror");
		img_mirror.setAttribute("class", "canvas__mirror");
		img_mirror.width = win_width;
		img_mirror.height = win_height;

		los_canvas_dom[0].appendChild(img_mirror);
		var canvas_dom_1 = document.getElementById("canvas0");

		ctx = canvas_dom_1.getContext("2d");
		ctx.strokeStyle = color;
		ctx.lineWidth = 3;

		los_canvas0.drawTouch(ctx);
		los_canvas0.drawPointer(ctx);
		los_canvas0.drawMouse(ctx);

		var button1 = document.querySelector('.js-btn__descargar__1');

		if (button1)
		{
			var button1click = debounce(function () {
				if ( !isiPad )
				{
					var dataURL1 = canvas_dom_1.toDataURL("image/png").replace("image/png", "image/octet-stream");
					button1.href = dataURL1;
				}
				else
				{
					// Falta ajustar que no abra una nueva ventana en ipad
					return false;
				}
			}, 600);
			button1.addEventListener('click', button1click);
		}

	}	
}
*/

function hasClass(el, cls)
{
	return el.className && new RegExp("(\\s|^)" + cls + "(\\s|$)").test(el.className);
}

/*
function findParentClass(parentClass, childObj)
{
	var testObj = childObj.parentNode;

	while( !hasClass(testObj, parentClass) )
	{
		testObj = testObj.parentNode;
	}
	return testObj;
}
*/

// <-- Generales

/*
function estructura()
{
	// nivel superior
	var num_diapositivas = contenedor.childElementCount;
	contenedor.style.width = ancho_pag * num_diapositivas + "px";
}
*/

// Multimedia -->

function medios()
{

	var audio = diapo_actual.querySelector('audio');
	var video = diapo_actual.querySelector('video');
	if ( audio || video )
	{
		
		var medio = (audio) ? audio : video;

		var media__control = medio.previousElementSibling;

		if ( media__control )
		{
			var media__controlclick = debounce(function () {
				play(medio);
			}, 600);
			media__control.addEventListener('click', media__controlclick);
		}

		// playAud(medio);
		// pauseAud(medio);
		
	}

	/*
	Play / Pausa a un audio en específico
	function playAud(med) {
		med.play();
	}

	function pauseAud(med) {
		med.pause();
	} 
	
	function playPause(medio)
	{

		if ( medio.paused )
		{
			medio.play(); 
		}
		else
		{
			medio.pause();
		}
	}
	*/
	function play(medio)
	{

		medio.play();
	}
}

// <- Multimedia
var c_regresiva = null;
function temporizador ()
{

	var tempor = diapo_actual.querySelector(".js-temporizador");

	if (tempor)
	{
		var segundero = tempor.querySelector(".js-segundero");
		var minutos = segundero.getAttribute('data-mins');
		var minutos_original = minutos;

		minutos = parseInt(minutos);
		var segundos = segundero.getAttribute('data-secs');
		var segundos_original = segundos;

		segundos = parseInt(segundos);
		var segundos_txt = ( segundos <= 9 ) ? "0"+segundos : segundos;
		
		var tiempo = minutos + ":" + segundos_txt;

		segundero.innerHTML = tiempo;

		var apagador = null;
		var timer = null;
		var alarma = null;

		Array.prototype.filter.call( tempor.parentNode.children, function(child)
		{
			if( child !== tempor && hasClass(child, "js-audio-timer") )
			{
				timer = child;
			}
			if( child !== tempor && hasClass(child, "js-audio-alarma") )
			{
				alarma = child;
			}
		});

		if (tempor)
		{
			var temporclick = debounce(function () {
				if ( !apagador )
				{
					// Si no hay contador corriendo
					tempor.classList.remove("terminado");
					tempor.classList.remove("acabando");
					minutos = segundero.getAttribute('data-mins');
					minutos = parseInt(minutos);
					segundos = segundero.getAttribute('data-secs');
					segundos = parseInt(segundos);
					apagador = 1;

					timer.play();

					if (c_regresiva)
					{
						window.clearInterval(c_regresiva);
					}

					c_regresiva =  window.setInterval(function(){

						if ( segundos <= 0)
						{
							minutos--;
							segundos = 59;
							segundos_txt = ""+segundos;
						}
						else 
						{
							segundos--;
							segundos_txt = ""+segundos;

							if (segundos <= 9 ){
								segundos_txt = "0"+segundos;
								if (minutos === 0 )
								{
									tempor.classList.add("acabando");
									setTimeout(function(){
										tempor.classList.remove("acabando");
									}, 300);
								}
							}
						}
						
						tiempo = minutos + ":" + segundos_txt;
						segundero.innerHTML = tiempo;

						if ( segundos <= 0 && minutos <= 0 )
						{
							timer.pause();
							alarma.play();
							window.clearInterval(c_regresiva);
							// timer = null;
							setTimeout(function(){
								tempor.classList.add("terminado");
							}, 300);
						}

					}, 1000);

				}
				else
				{
					// Si ya hay un contador corriendo
					window.clearInterval(c_regresiva);

					minutos = minutos_original;
					segundos = segundos_original;
					minutos = parseInt(minutos);
					segundos = parseInt(segundos);
					tempor.classList.remove("terminado");
					tempor.classList.remove("acabando");

					segundos_txt = ( segundos <= 9 ) ? "0"+segundos : segundos;

					tiempo = minutos + ":" + segundos_txt;

					segundero.innerHTML = tiempo;

					apagador = null;
					timer.pause();
					alarma.pause();
				}
			}, 600);
			tempor.addEventListener('click', temporclick);
		}
	}
}

function sonidos () {

	var tempos = document.querySelectorAll(".js-temporizador");
	var timer = document.querySelector(".js-audio-timer");
	var alarma = document.querySelector(".js-audio-alarma");

	Array.prototype.forEach.call(tempos, function(temp, i){

		var nuevo_timer = timer.cloneNode(true);
		nuevo_timer.setAttribute("id", "audio__timer_" + i);
		insertAfter(nuevo_timer, temp);

		var nuevo_alarma = alarma.cloneNode(true);
		nuevo_alarma.setAttribute("id", "audio__alarma_" + i);
		insertAfter(nuevo_alarma, temp);
	});
}

// Resize -->

function rsize()
{
	// ancho_pag = document.body.offsetWidth;
	// // nivel superior
	// var num_diapositivas = contenedor.childElementCount;

	// contenedor.style.width = ancho_pag * num_diapositivas + "px";

	/*
	var esta_seccion = document.querySelector("[data-ir='"+diapo_visible+"']");
	var event = document.createEvent('HTMLEvents');
	event.initEvent('click', true, false);
	if (esta_seccion)
	{
		esta_seccion.dispatchEvent(event);
	}
	*/

	// newCanvas();
}

window.onresize = function()
{
	if (tOut !== false)
	{
		clearTimeout(tOut);
	}
	tOut = setTimeout(rsize, milSec);
};

// <-- Carrusel

function fullScreen()
{
	var apagador = 0;
	function launchFullScreen(element) {
		if(element.requestFullScreen) {
			element.requestFullScreen();
		} else if(element.mozRequestFullScreen) {
			element.mozRequestFullScreen();
		} else if(element.webkitRequestFullScreen) {
			element.webkitRequestFullScreen();
		} else if(element.msRequestFullScreen) {
			element.msRequestFullScreen();
		}
	}
	function exitFullscreen() {
		if(document.exitFullscreen) {
			document.exitFullscreen();
		} else if(document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		} else if(document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
	}

	var btn_fullScreen = document.querySelector(".js-fullScreen");
	if (btn_fullScreen)
	{
		var btn_fullScreenclick = debounce(function () {
			if (apagador === 0)
			{
				launchFullScreen(document.documentElement);
				apagador = 1;
			} else {
				exitFullscreen();
				apagador = 0;
			}
			rsize();
		}, 600);
		btn_fullScreen.addEventListener('click', btn_fullScreenclick);
	}
}

// Menú y navegación -->

function mov_menu()
{
	btn_menu.classList.toggle("open");
	menu.classList.toggle("visible");
	el_menu.classList.toggle("visible");
}

function menu_func()
{
	if (btn_menu)
	{
		var btn_menuclick = debounce(function () {
			mov_menu();
		}, 600);
		btn_menu.addEventListener('click', btn_menuclick);
	}
}
/*
function diapo_actual_func () {
	Array.prototype.forEach.call(secciones, function(el){
		if( el !== diapo_actual && hasClass(el, "actual") )
		{
			el.classList.remove("actual");
		}
	});
	diapo_actual.classList.add("actual");
}
*/
/*

function menu_activo_func()
{
	var menu_activo = menu.querySelector("[data-seccion="+diapo_visible+"]");
	if (menu_activo)
	{
		Array.prototype.filter.call(menu_activo.parentNode.children, function(child){
			if( child !== menu_activo  && hasClass(child, "actual") )
			{
				child.classList.remove("actual");
			}
		});
		menu_activo.classList.add("actual");
	}
}

*/

function navegacion()
{
	var btns_nav = document.querySelectorAll('.js-nav');

	Array.prototype.forEach.call(btns_nav, function( el )
	{
		if ( el )
		{
			var elclick = debounce(function ()
			{
				diapo_visible = el.getAttribute('data-ir');

				if (diapo_visible)
				{
					// Mover carrusel
					// var diapo = index_en_array( diapositivas, diapo_visible);
					// setTimeout(function()
					// {
					// 	contenedor.style.left = (diapo * ancho_pag) * ( - 1 ) + "px";
					// },250);
					// Menú activo
					/*
					menu_activo_func();

					diapo_actual = contenedor.querySelector("[data-seccion="+diapo_visible+"]");

					if (diapo_actual)
					{
						// Diapo actual
						diapo_actual_func();
					}
					*/
				}

				// Funciones dependiendo de la sección

				/*
				if (diapo_visible === "mapa" || diapo_visible === "encauzamiento" )
				{
					newCanvas();
				}
				*/

				// Comenzar lluvia en simulador si está visible
				if (diapo_visible === "simulador_1" || diapo_visible === "encauzamiento" )
				{
					for (var i = 0, len = los_simuladores.length; i < len; i++)
					{
						if (los_simuladores[i].parentNode === diapo_actual)
						{
							simulador[i].lluvia(i);
						}
					}

				}

				setTimeout(function()
				{
					el_menu.style.display = "block";
					btn_menu.classList.remove("internas");
				}, 250);

				if (  el_menu.classList.contains("visible") )
				{
					mov_menu();
				}
			}, 600);
			el.addEventListener('click', elclick);
		}
	});
}

// <-- Menú y navegación


// Simulador -->

function simuladores () {

	// Crear Objeto simulador
	function Simulador(escena, riesgo, i) {
		this.escena = escena;
		this.riesgo = riesgo;
		this.numArboles = 0;

		// Funcionalidad de los controles.
		// Obtener todos los botoenes de control y asignar función al click
		this.controles = function() {

			var los_controles = this.escena.querySelectorAll(".js-simulador--control");
			var click = document.querySelector(".js-audio-click");

			Array.prototype.forEach.call(los_controles, function(cntrl){

				var accion = cntrl.getAttribute("data-accion");

				if (cntrl)
				{
					var cntrlclick = debounce(function () {
						simulador[i].accionNombre = cntrl.getAttribute("data-accionNombre");

						if ( accion === "neg")
						{
							simulador[i].contador("mas", i);
						}
						else if ( accion === "pos")
						{
							simulador[i].contador("menos", i);
						}

						click.play();
					}, 600);
					cntrl.addEventListener('click', cntrlclick);
				}

			});
		};

		// Funcionalidad del contador de riesgo.
		// Sumar o restar riesgo de acuerdo al evento que se presente (click de controles o lluvia)
		this.contador = function(valor, i) {

			var limite = 10;

			if ( simulador[i].riesgo >= 1 && valor === "menos" )
			{
				simulador[i].riesgo--;
				simulador[i].animacion(simulador[i].riesgo);
			}
			if ( simulador[i].riesgo <= (limite-1) && valor === "mas")
			{
				simulador[i].riesgo++;
				simulador[i].animacion(simulador[i].riesgo);
			}

			if ( simulador[i].riesgo === limite )
			{
				simulador[i].deslizamiento(i);
			}
		};

		// Funcionalidad de la animación
		// Animar todos los elementos del escenario de acuerdo al contador
		this.animacion = function(riesgo) {

			// Árboles

			function talarReforestar (acc) {

				if ( acc === "Talar" )
				{
					switch(simulador[i].numArboles)
					{
						case 0 :
							simulador[i].arboles[0].style.display = "none";
							simulador[i].arboles[1].style.display = "none";
							break;
						case 1 :
							simulador[i].arboles[3].style.display = "none";
							simulador[i].arboles[4].style.display = "none";
							break;
						case 2 :
							simulador[i].arboles[5].style.display = "none";
							simulador[i].arboles[6].style.display = "none";
							break;
						case 3 :
							simulador[i].arboles[2].style.display = "none";
							simulador[i].arboles[7].style.display = "none";
							break;
					}
					if ( simulador[i].numArboles <= 3 )
					{
						simulador[i].numArboles++;
					}
				}

				if ( acc === "Reforestar" )
				{
					switch(simulador[i].numArboles)
					{
						case 4 :
							simulador[i].arboles[2].style.display = "block";
							simulador[i].arboles[7].style.display = "block";
							break;
						case 3 :
							simulador[i].arboles[5].style.display = "block";
							simulador[i].arboles[6].style.display = "block";
							break;
						case 2 :
							simulador[i].arboles[3].style.display = "block";
							simulador[i].arboles[4].style.display = "block";
							break;
						case 1 :
							simulador[i].arboles[0].style.display = "block";
							simulador[i].arboles[1].style.display = "block";
							break;
					}
					if ( simulador[i].numArboles >= 1 )
					{
						simulador[i].numArboles--;
					}
				}
			}

			simulador[i].arboles = simulador[i].escena.querySelectorAll(".js-simulador--arbol");

			var accion_nom = simulador[i].accionNombre;

			if ( accion_nom === "Talar" || accion_nom === "Reforestar" )
			{
				talarReforestar(accion_nom, i);
			}


			// Flechas
			var flecha__fuerza = simulador[i].escena.querySelector(".js-simulador--fuerza");
			var flecha__carga = simulador[i].escena.querySelector(".js-simulador--carga");
			flecha__fuerza.setAttribute("data-tamanio", 10-riesgo);
			flecha__carga.setAttribute("data-tamanio", 10+riesgo);

			// Escala
			var niveles = simulador[i].escena.querySelectorAll(".js-simulador--nivel");
			var index = simulador[i].escena.querySelector("[data-simuladorIndex='"+riesgo+"']");

			Array.prototype.forEach.call(niveles, function(el){
				if( hasClass(el, "activo") )
				{
					el.classList.remove("activo");
				}
			});

			index.classList.add("activo");

		};

		// Funcionalidad de la lluvia
		// Animación de la lluvia y modificación del contador en lapsos random de tiempo
		this.lluvia = function(i) {

			simulador[i].t_lluvia = null;
			var lluvia_contador = 30000;
			var la_lluvia = simulador[i].escena.querySelector(".js-simulador--lluvia");
			var fx_lluvia = document.querySelector(".js-audio-lluvia");

			function random(min, max) {
				var randomInt = Math.floor(Math.random() * (max - min)) + min;
				randomInt = randomInt * 100;
				return randomInt;
			}

			if (simulador[i].t_lluvia)
			{
				clearInterval(simulador[i].t_lluvia);
			}

			function llueve(){

				console.log("¡Llueve!");
				la_lluvia.classList.add("llueve");
				fx_lluvia.play();

				setTimeout(function(){
					la_lluvia.classList.remove("llueve");
				}, 3000);

				simulador[i].contador("mas", i);
				clearInterval(simulador[i].t_lluvia);
				simulador[i].t_lluvia =  setInterval(llueve, random(300, 1000) );
			}

			simulador[i].t_lluvia =  setInterval(llueve, lluvia_contador );

		};

		// Función Once
		// Para controlar que no se repita un deslizamiento dos veces en un escenario.
		function once(fn, context) {
			var result;

			return function() {
				if(fn) {
					result = fn.apply(context || this, arguments);
					fn = null;
				}

				return result;
			};
		}

		// Funcionalidad del deslizamiento
		// Animación del deslizamiento cuando el riesgo llegue a 10.
		// Sólo se dispara una vez por escenario.
		this.deslizamiento = once ( function(i) {
			setTimeout(function(){

				var el_deslizamiento = simulador[i].escena.querySelector(".js-simulador--deslizamiento");
				var simulador_controles = simulador[i].escena.querySelector(".js-simulador--controles");
				var simulador_escena = simulador[i].escena.querySelector(".js-simulador--escena");
				var simulador_escala = simulador[i].escena.querySelector(".js-simulador--escala");
				var simulador_leyendas = simulador[i].escena.querySelector(".js-simulador--leyendas");

				var audio_deslizamiento = document.querySelector(".js-audio-deslizamiento");

				var fx_alarmafuego = document.querySelector(".js-audio-alarmafuego");

				el_deslizamiento.classList.add("visible");

				simulador_controles.style.display = "none";
				simulador_escena.style.display = "none";
				simulador_escala.style.display = "none";
				simulador_leyendas.style.display = "none";

				fx_alarmafuego.play();
				el_deslizamiento.currentTime = 0;
				el_deslizamiento.pause();

				setTimeout( function() {
					el_deslizamiento.play();
					audio_deslizamiento.play();
				}, 3000);

				clearInterval(simulador[i].t_lluvia);
			}, 300);

		});

	}

	// Obtener todos los escenarios de simuladores, construcción de objeto e inicializar funciones de controles y lluvia.

	Array.prototype.forEach.call(los_simuladores, function(sim,i){

		var riesg = sim.getAttribute("data-riesgo");
		riesg = parseInt(riesg);

		simulador[i] = new Simulador(sim, riesg, i);
		simulador[i].controles();
		simulador[i].lluvia(i);
	});
}

// <-- Simulador

// Mapa comunitario -->

function mapa_comunitario () {
	var mapa__original = document.querySelector(".js-mapa__original");
	if (mapa__original)
	{
		var mapa__originalclick = debounce(function () {
			this.style.display = "none";
		}, 600);
		mapa__original.addEventListener('click', mapa__originalclick);
	}
}

// <-- Mapa comunitario

// Saber es poder -->

function saber__poder () {
	var saber__intro = document.querySelector(".js-saber--intro");
	var saber__salida = document.querySelector(".js-saber--salida");

	var saber__niveles = document.querySelectorAll(".js-saber--nivel");
	var saber__fuentes = document.querySelectorAll(".js-saber--fuente");

	var fuentes = 0;

	// Función de primer audio Intro
	if (saber__intro)
	{
		var saber__introclick = debounce(function ()
		{
			var audio_intro = this.querySelector("audio");
			audio_intro.play();
			audio_intro.addEventListener('ended', function()
			{
				setTimeout(function()
				{
					window.location = "informacion2.html";
				}, 2000);
			});
		}, 600);
		saber__intro.addEventListener('click', saber__introclick);
	}

	// Función de último audio
	if (saber__salida)
	{
		var saber__salidaclick = debounce(function ()
		{
			var audio_salida = this.querySelector("audio");
			audio_salida.play();
		}, 600);
		saber__salida.addEventListener('click', saber__salidaclick);
	}

	// Función efecto de entrada de audio fuente
	function entrada_fuente (fuente, audio_fuente) {

		Array.prototype.forEach.call(saber__fuentes, function(fuu){
			if( fuu !== fuente )
			{
				fuu.classList.add("minimizar");
			}
		});

		fuente.classList.add("maximizar");

		setTimeout(function(){
			audio_fuente.play();
		}, 300);
	}

	// Función efecto de salida de audio fuente
	function salida_fuente (fuente) {
		
		Array.prototype.forEach.call(saber__fuentes, function(fuu){
			if( fuu !== fuente )
			{
				fuu.classList.remove("minimizar");
			}
		});

		fuente.classList.remove("maximizar");

		setTimeout(function(){
			fuente.classList.add("desactivado");
		}, 300);

	}

	Array.prototype.forEach.call(saber__fuentes, function(fuente)
	{
		if (fuente)
		{
			var fuenteclick = debounce(function ()
			{
				
				var audio_fuente = this.querySelector("audio");
				entrada_fuente(fuente, audio_fuente);

				audio_fuente.addEventListener('ended', function()
				{
					salida_fuente(fuente);

					saber__niveles[fuentes].classList.add("consultado");

					fuentes++;
					console.log(fuentes);

					if ( fuentes === 4 )
					{
						setTimeout(function(){

							window.location = "informacion3.html";

						}, 5000);
					}
				});
			}, 600);
			fuente.addEventListener('click', fuenteclick);

		}
	});
}

// <-- Saber es poder

document.addEventListener('DOMContentLoaded', function()
{
	// estructura();
	menu_func();
	fullScreen();
	navegacion();
	simuladores();
	mapa_comunitario();
	sonidos();
	saber__poder();
	ipad_function();
	// Checar si diapo tiene audio o video
	medios();
	// Disparar temporizador
	temporizador();
});